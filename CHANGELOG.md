CHANGELOG
=========

2020-09-18 (0.4.1)
------------------

- `number_of_rounds` in `RepChainDataframeHolder` can now be either float or int (instead of just int).

2020-08-31 (0.4.0)
------------------

- Using NetSquid's BellIndex for swap/midpoint outcomes.
- Fixed bug in data type/value checking of RepChainDataFrameHolder (it never raised an exception).
- `repchain_data_plot.py` contains standardized methods to plot QKD data and fidelity against varied parameter.
- Method for processing fidelity data that can then be used for plotting is added.

2020-07-28 (0.3.0)
------------------

- version info of used packages is now available in RepChainDataFrameHolder

2020-07-22 (0.2.0)
------------------

- cutoff time can now be retroactively implemented on results of single-repeater simulations

2020-06-08 (0.1.0)
------------------

- attenuation coefficient can now be specified when calculating secret-key rate capacity
- error bars on QBER can now be scaled depending on desired confidence interval
- secret key rate now has min/max and symmetric error bars
- added several utility function to plotting (e.g. fitting)


2020-01-28 (0.0.5)
------------------

- added _NOT_SPECIFIED to ParameterSet


2020-01-27 (0.0.4)
------------------

- hot-fix to repchain_sampler, fixing comparison of large states failing


2020-01-22 (0.0.3)
------------------

- added repchain_sampler


2019-12-25 (0.0.2)
------------------

- added function to calculate end-to-end fidelity 
- small fix to parameter set class to add exception for "infinite integers"


2019-11-29 (0.0.1)
------------------

- added repchain_dataframe_holder and associated functions


2019-11-29 (0.0.0)
------------------

- Created this snippet
