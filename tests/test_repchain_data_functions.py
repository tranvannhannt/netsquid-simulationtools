import unittest

import pandas
import numpy as np
from statistics import mean, stdev
from netsquid.qubits.ketstates import BellIndex

from netsquid.qubits.ketstates import b00, b01, b10, b11
from netsquid.qubits.operators import I, X, Y, Z

from netsquid_simulationtools.repchain_data_functions import _expected_target_state, _do_we_expect_correlation,\
    agreement_with_expected_outcome, qber, _secret_key_rate, _binary_entropy, end_to_end_fidelity,\
    secret_key_rate_from_outcomes


class TestProcessingFunctions(unittest.TestCase):
    """Unittests to verify the functionality of the processing tools for the repchain_dataframe_holder."""

    @staticmethod
    def bell_state(index):
        """static method to get KetState corresponding to bell index."""
        bell_index_to_state = {BellIndex.B00: b00,
                               BellIndex.B01: b01,
                               BellIndex.B11: b11,
                               BellIndex.B10: b10,
                               }
        return bell_index_to_state[index]

    @staticmethod
    def pauli_correction(index):
        # switched version for applying Pauli to A in stead of B
        bell_index_to_correction = {BellIndex.PHI_PLUS: I ^ I,
                                    BellIndex.PSI_PLUS: I ^ X,
                                    BellIndex.PSI_MINUS: I ^ Y,
                                    BellIndex.PHI_MINUS: I ^ Z,
                                    }
        return bell_index_to_correction[index]

    @staticmethod
    def operator(index):
        bell_index_to_operator = {BellIndex.PHI_PLUS: I,
                                  BellIndex.PSI_PLUS: X,
                                  BellIndex.PSI_MINUS: Y,
                                  BellIndex.PHI_MINUS: Z,
                                  }
        return bell_index_to_operator[index]

    @staticmethod
    def SimData():
        data = pandas.DataFrame({"basis_A": [],
                                 "basis_B": [],
                                 "outcome_A": [],
                                 "outcome_B": [],
                                 "number_of_rounds": [],
                                 "midpoint_outcome_0": [],
                                 "midpoint_outcome_1": [],
                                 "midpoint_outcome_2": [],
                                 "swap_outcome_0": [],
                                 "swap_outcome_1": [],
                                 "state": [],
                                 })
        #params = {}
        return data

    def test_expected_target_state_one_midpoint_outcome(self):
        for bell_index in BellIndex:
            row = pandas.Series({"midpoint_outcome_0": bell_index})
            assert _expected_target_state(row) == bell_index

    def test_expected_target_state_one_swap_outcome(self):
        for bell_index in BellIndex:
            row = pandas.Series({"swap_outcome_0": bell_index})
            assert _expected_target_state(row) == bell_index

    def test_expected_target_state(self):
        """Tests Correctness of expected target state first, as other functions are based on this function."""
        data = self.SimData()

        # see if we labelled the Bell states correctly :)
        self.assertTrue((self.bell_state(0) == (self.pauli_correction(0) * b00).arr).all())
        self.assertTrue((self.bell_state(1) == (self.pauli_correction(1) * b00).arr).all())
        self.assertTrue((self.bell_state(2) == - 1j * (self.pauli_correction(2) * b00).arr).all())
        self.assertTrue((self.bell_state(3) == (self.pauli_correction(3) * b00).arr).all())

        # fill outcomes in Dataframe with all possible combinations
        outcome_list = []
        for a in range(4):
            for b in range(4):
                for c in range(4):
                    for d in range(4):
                        for e in range(4):
                            outcome_list.append([a, b, c, d, e])
        for i in range(len(outcome_list)):
            data.loc[i] = ["X", "X", 1, 1, int(1)] + outcome_list[i] + [1]

        # compare state that is predicted by _expected_target_state with state calculated with pauli corrections
        for i in range(len(outcome_list)):
            expected_index = _expected_target_state(data.iloc[i])
            ii = outcome_list[i].count(0)
            x = outcome_list[i].count(1)
            y = outcome_list[i].count(2)
            z = outcome_list[i].count(3)
            pauli_corrected_state = np.linalg.matrix_power(self.pauli_correction(0).arr, ii) @ \
                np.linalg.matrix_power(self.pauli_correction(1).arr, x) @ \
                np.linalg.matrix_power(self.pauli_correction(2).arr, y) @ \
                np.linalg.matrix_power(self.pauli_correction(3).arr, z) @ b00
            try:
                self.assertTrue((pauli_corrected_state == self.bell_state(expected_index)).all())
            except AssertionError:
                # global complex phase but still the same state
                self.assertTrue((pauli_corrected_state == 1j * self.bell_state(expected_index)).all())

    def test_expected_correlations_unequal_bases(self):
        row = pandas.Series({"basis_A": "Z", "basis_B": "X"})
        with self.assertRaises(ValueError):
            _do_we_expect_correlation(row)

    def test_expected_correlations(self):
        """Tests Correctness of expected correlation for different states and basis choices."""
        # create list that contains each pauli correction once
        outcome_list = []
        for i in range(4):
            outcome_list.append([i, 0, 0, 0, 0])
        # basis dictionary
        basis = {1: "X",
                 2: "Y",
                 3: "Z"}
        # loop over all three basis
        for basis_index in [1, 2, 3]:
            data = self.SimData()

            # fill dataframe to get each bellstate once
            for l in range(len(outcome_list)):
                data.loc[l] = [basis[basis_index], basis[basis_index], 1, 1, 1] + outcome_list[l] + [1]

            # check if predicted correlations agree with expectation value of state
            for i in range(len(outcome_list)):
                do_we_expect_corr = _do_we_expect_correlation(data.iloc[i])
                bell_state = self.bell_state(_expected_target_state(data.iloc[i]))

                bell_state_c = np.conjugate(bell_state)
                bell_state_T = np.transpose(bell_state_c)
                meas, = bell_state_T @ (self.operator(basis_index) ^ self.operator(basis_index)).arr @ bell_state
                if int(round(meas[0])) == -1:
                    self.assertFalse(do_we_expect_corr)
                else:
                    self.assertTrue(do_we_expect_corr)

            # while we have the dataframe, test agreement and qber
            agreement_list = agreement_with_expected_outcome(data, basis[basis_index])
            qb, qber_error = qber(data, basis[basis_index])
            self.assertTrue(mean(agreement_list) == 0.5)
            self.assertTrue(qb == 0.5)
            # TODO: think about whether qber should return stdev or error
            self.assertTrue(qber_error == stdev(agreement_list) / np.sqrt(len(agreement_list)))

    '''def test_agreement_with_expectation(self):
        """Tests Correctness of agreement with expected outcome."""
        # create list that contains each pauli correction once
        outcome_list = []
        for i in range(4):
            outcome_list.append([i, 0, 0, 0, 0])'''

    def test_secret_key_rate(self):
        """Tests Correctness of calculated secret key rate."""

        for qber_x in np.linspace(0, 1, 100):
            for qber_z in np.linspace(0, 1, 100):
                skr, skr_min, skr_max, skr_error = _secret_key_rate(qber_x, 0., qber_z, 0., 1, 0)
                self.assertEqual((max(0., 1 - _binary_entropy(qber_x) - _binary_entropy(qber_z))), skr)
                for i in [skr, skr_min, skr_max]:
                    self.assertTrue(max(i, 1.) == 1.)
                    self.assertTrue(min(i, 0.) == 0.)

        data = self.SimData()
        # fill outcomes in Dataframe with all possible combinations
        outcome_list = []
        for a in range(4):
            for b in range(4):
                for c in range(4):
                    for d in range(4):
                        for e in range(4):
                            outcome_list.append([a, b, c, d, e])
        for i in range(len(outcome_list)):
            data.loc[i] = ["X", "X", 1, 1, int(1)] + outcome_list[i] + [1]
        secret_key_rate, skr_min, skr_max, skr_error = secret_key_rate_from_outcomes(data)
        self.assertEqual(secret_key_rate, 0.)

    def test_end_to_end_fidelity(self):
        """Tests correct implementation of the end-to-end fidelity calculation."""

        # test functionality with perfect bell states
        for i in range(4):
            data = {"state": [self.bell_state(i)]*10,
                    "midpoint_outcome_0": [1]*10,
                    "number_of_rounds": [1]*10,
                    }
            df = pandas.DataFrame(data)

            f, f_err = end_to_end_fidelity(df)

            if i == 1:
                self.assertAlmostEqual(f, 1.)
            else:
                self.assertAlmostEqual(f, 0.)
            self.assertAlmostEqual(f_err, 0.)


if __name__ == "__main__":
    unittest.main()
