import math
import os
import pickle
import random
import shutil
import unittest

import netsquid.qubits.ketstates as ketstates
import numpy as np
import pandas

from netsquid_simulationtools.repchain_data_process import process_qkd_data, process_container_qkd, \
    secret_key_capacity, process_fidelity_data, process_fidelity_container
from netsquid_simulationtools.repchain_dataframe_holder import RepchainDataFrameHolder


class TestRepchainDataProcess(unittest.TestCase):
    """Unittests to verify the functionality of `repchain_dataframe_process`."""

    test_data_dir = "tests/test_data"

    def setUp(self):
        """Creates mock dataframes for testing purposes."""
        self.number_of_results = 6

        baseline_parameters_dict = {"probability": 0.42, "fidelity": 0.42}

        bell_states = [np.outer(ketstates.b00, ketstates.b00), np.outer(ketstates.b01, ketstates.b01),
                       np.outer(ketstates.b10, ketstates.b10), np.outer(ketstates.b11, ketstates.b11)]

        different_bases = {
            "state": random.choices(population=bell_states, k=self.number_of_results),
            "basis_A": ["X", "Y", "Z", "X", "Y", "Z"],
            "basis_B": ["Y", "X", "Y", "Z", "X", "Y"],
            "outcome_A": [0, 1, 0, 1, 1, 1],
            "outcome_B": [1, 0, 1, 0, 1, 0],
            "number_of_rounds": [1, 1, 1, 1, 1, 1],
            "midpoint_outcome_0": [0, 1, 2, 3, 0, 1],
            "midpoint_outcome_1": [3, 2, 1, 0, 3, 2],
            "swap_outcome_0": [0, 1, 2, 3, 0, 1],
            "length": [1, 2, 3, 4, 5, 6]
        }

        same_bases = {
            "state": random.choices(population=bell_states, k=self.number_of_results),
            "basis_A": ["X", "X", "Z", "Z", "Z", "Z"],
            "basis_B": ["X", "X", "Z", "Z", "Z", "Z"],
            "outcome_A": [0, 1, 0, 1, 0, 1],
            "outcome_B": [1, 0, 1, 0, 1, 0],
            "number_of_rounds": [1, 1, 1, 1, 1, 1],
            "midpoint_outcome_0": [0, 1, 2, 3, 0, 2],
            "midpoint_outcome_1": [3, 2, 1, 0, 3, 1],
            "swap_outcome_0": [0, 0, 0, 0, 0, 0],
            "length": [1, 1, 2, 2, 3, 3]
        }

        fidelity = {
            "state": [bell_states[1]] * 6,
            "number_of_rounds": [1] * 6,
            "midpoint_outcome_0": [1] * 6,
            "midpoint_outcome_1": [1] * 6,
            "swap_outcome_0": [1] * 6,
            "length": [1, 1, 2, 2, 3, 3]
        }

        self.mock_a = RepchainDataFrameHolder(number_of_nodes=3,
                                              baseline_parameters=baseline_parameters_dict,
                                              data=different_bases)

        self.mock_b = RepchainDataFrameHolder(number_of_nodes=3,
                                              baseline_parameters=baseline_parameters_dict,
                                              data=same_bases)

        self.mock_fid = RepchainDataFrameHolder(number_of_nodes=3,
                                                baseline_parameters=baseline_parameters_dict,
                                                data=fidelity)

        if not os.path.exists(self.test_data_dir):
            os.makedirs(self.test_data_dir)

        pickle.dump(self.mock_a, open(self.test_data_dir + "/mock_dataframe_qkd.pickle", "wb"))
        pickle.dump(self.mock_fid, open(self.test_data_dir + "/mock_dataframe_fidelity.pickle", "wb"))

    def tearDown(self):
        """Deletes test data directory with all files in it."""
        shutil.rmtree(self.test_data_dir)

    def test_secret_key_capacity_positive_length(self):
        """Check results for secret key capacity with positive length."""

        attenuation = 5 / (11 * np.log(10))  # corresponds to attenuation length of 22 km
        sk_capacity, tgw_bound = secret_key_capacity(length=4.2, attenuation=attenuation)
        self.assertAlmostEqual(sk_capacity, 2.5245638855922863)
        self.assertAlmostEqual(tgw_bound, 3.3934147378943593)

    def test_secret_key_capacity_zero_length(self):
        """Check results for secret key capacity with zero length and values very close to zero."""
        cap1, bound1 = secret_key_capacity(0)
        cap2, bound2 = secret_key_capacity(10e-16)

        # check results are equal
        self.assertEqual(cap1, cap2)
        self.assertEqual(bound1, bound2)

        # check values of results
        self.assertEqual(cap1, 13.287712379549609)
        self.assertEqual(bound1, 14.287640242994135)

    def test_secret_key_capacity_negative_length(self):
        """Check results for secret key capacity with negative length."""
        with self.assertRaises(ValueError):
            _, _ = secret_key_capacity(-1.0)

    def test_process_container_qkd_different_bases(self):
        """Check that data is processed correctly for the trivial case where sk rate is 0."""
        processed_data = process_container_qkd(self.mock_a)

        nans = [math.nan for _ in range(self.number_of_results)]
        sk_cap = []
        tgw_bound = []
        for i in range(1, 7):
            sk, tgw = secret_key_capacity(length=i)
            sk_cap.append(sk)
            tgw_bound.append(tgw)

        self.assertEqual(len(processed_data), self.number_of_results)

        self.assertTrue((processed_data.norm_sk_rate == 0).all())
        self.assertTrue((processed_data.norm_sk_rate_upper_bound == 0).all())
        self.assertTrue((processed_data.norm_sk_rate_lower_bound == 0).all())

        self.assertTrue((processed_data.sk_rate == 0).all())
        self.assertTrue((processed_data.sk_rate_upper_bound == 0).all())
        self.assertTrue((processed_data.sk_rate_upper_bound == 0).all())

        self.assertTrue((processed_data.Qber_z == 0).all())
        self.assertTrue((processed_data.Qber_z_error == 0).all())
        self.assertTrue((processed_data.Qber_x == 0).all())
        self.assertTrue((processed_data.Qber_x_error == 0).all())

        self.assertTrue((processed_data.number_of_successes == 1).all())
        self.assertTrue((processed_data.attempts_per_success == 1).all())

        np.testing.assert_array_equal(processed_data.attempts_per_success_error.values, nans)
        np.testing.assert_array_equal(processed_data.sk_capacity.values, sk_cap)
        np.testing.assert_array_equal(processed_data.tgw_bound.values, tgw_bound)

    def test_process_container_qkd_same_bases(self):
        """Check that sk rate is calculated properly with perfect correlation."""
        processed_data = process_container_qkd(self.mock_b)

        self.assertEqual(len(processed_data), 3)  # there are three unique value of varied parameters

        self.assertTrue((processed_data.norm_sk_rate == 0.5).all())
        self.assertTrue((processed_data.norm_sk_rate_upper_bound == 0.5).all())
        self.assertTrue((processed_data.norm_sk_rate_lower_bound == 0.5).all())

        self.assertTrue((processed_data.sk_rate == 1).all())
        self.assertTrue((processed_data.sk_rate_upper_bound == 1).all())
        self.assertTrue((processed_data.sk_rate_lower_bound == 1).all())

    def test_process_qkd_data(self):
        """Check that correct output files for processed data are created."""
        process_qkd_data(raw_data_dir=self.test_data_dir, suffix="qkd.pickle",
                         output=self.test_data_dir + "/repchain_data_process_qkd_results.pickle",
                         csv_output_files=(self.test_data_dir + "/first_csv_output.csv",
                                           self.test_data_dir + "/second_csv_output.csv"),
                         plot_processed_data=False)

        df1 = pandas.read_csv(self.test_data_dir + "/first_csv_output.csv")
        df2 = pandas.read_csv(self.test_data_dir + "/second_csv_output.csv")

        self.assertEqual(df1.shape, (self.number_of_results, 18))
        self.assertEqual(df2.shape, (self.number_of_results, 2))

    def test_process_container_fidelity(self):
        """Check that fidelity and fidelity error is calculated properly."""
        processed_data = process_fidelity_container(self.mock_fid)

        self.assertEqual(len(processed_data), 3)

        self.assertTrue((np.isclose(processed_data.fidelity, 1.0)).all())
        self.assertTrue((np.isclose(processed_data.fidelity_error, 0.0)).all())

    def test_process_fidelity_data(self):
        """Check that the output file holds correct number of datapoints."""
        process_fidelity_data(raw_data_dir=self.test_data_dir, suffix="fidelity.pickle",
                              output=self.test_data_dir + "/repchain_data_process_fid_results.pickle",
                              csv_output=self.test_data_dir + "/csv_output.csv", plot_processed_data=False)

        df = pandas.read_csv(self.test_data_dir + "/csv_output.csv")

        self.assertEqual(df.shape, (3, 5))
