import os
import time
import pickle


def combine_data(raw_data_dir="raw_data", suffix="_states.pickle",
                 output="combined_state_data.pickle", save_output_to_file=True):
    """Simply combine different RepchainDataFrameHolder objects with a common suffix without further processing.

    Parameters
    ----------
    raw_data_dir : str (optional)
        Directory containing the data (pickle files) to be combined.
    suffix : str (optional)
        Common ending of the files to be combined.
    output: str (optional)
        filename of combined file
    save_output_to_file : bool (optional)
        if True, saves output into a file (default) and returns the combined data
        if False, nothing is saved to a file and the combined data is only returned
    """
    start_time = time.time()

    # collect all data from folder, process and put together
    num_files = 0
    combined_data = None
    sf = len(suffix)
    if not os.path.exists(raw_data_dir):
        raise NotADirectoryError("No raw_data directory found!")
    for filename in os.listdir(raw_data_dir):
        if filename[-sf:] == suffix:
            if os.path.isfile("{}/{}".format(raw_data_dir, filename)):
                # read out RepChain Containers
                new_data = pickle.load(open("{}/{}".format(raw_data_dir, filename), "rb"))
                if combined_data is None:
                    combined_data = new_data
                else:
                    combined_data.combine(new_data, assert_equal_baseline_parameters=False)

                num_files += 1
    num_suc = len(combined_data.dataframe.index)

    if save_output_to_file is True:
        # save combined data as single pickle file
        pickle.dump(combined_data, open(output, "wb"))

    runtime = time.time() - start_time

    print("\n\nCombined {} files containing {} successes in {} seconds into output file {}."
          .format(num_files, num_suc, runtime, output))

    # returns the combined data
    return combined_data


if __name__ == "__main__":

    combine_data()
