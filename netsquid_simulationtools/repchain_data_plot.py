from argparse import ArgumentParser
import matplotlib.pyplot as plt
import pandas
import numpy as np
import os

from netsquid_simulationtools.repchain_data_functions import _secret_key_rate


def plot_qkd_data(filename, scan_param_name, scan_param_label, save_filename=None, shaded=True, skr_legend=None,
                  plot_skr_qber_att=(True, True, True), show_fit_lines=(True, True, True), skr_ylim=None,
                  qber_ylim=None, att_ylim=None, normalization=None, convert_to_per_second=False,
                  save_formats=(".eps", ".svg", ".pdf", ".png")):
    """Read a combined container with processed QKD data and plots secret-key rate, QBERs and number of attempts per
    success over the specified/varied parameter.

    Parameters
    ----------
    filename : str
        Filename of a csv file containing the combined processed QKD data and the varied parameter. This .csv file can
        be generated with :meth:`process_qkd_data` in `repchain_data_process.py`. The .csv file has to have the
        following columns: scan_param_name, 'sk_rate', 'sk_rate_lower_bound', 'sk_rate_upper_bound', 'Qber_x', 'Qber_z',
        and 'attempts_per_success'.
    scan_param_name : str
        Name of parameter that was varied over and should be plotted on the x-axis.
    scan_param_label : str
        Label of the parameter that was varied and should be plotted on the x-axis.
        For example if length was the varied parameter, its label would be `Total Length [km]`.
    save_filename : str (optional)
        Name of the file the figure should be saved as.
        Default is None which creates a name for the saved plot using the name of the input file (filename).
    shaded : Boolean (optional)
        Whether the SKR data should have shaded visualisation of the error.
    skr_legend : list of str (optional)
        Sets a legend for the SKR plot.
    plot_skr_qber_att : tuple of 3 Booleans (optional)
        Specifies which plots should be plotted (Secret Key Rate, QBER, attempts per success).
        By default, all three plots are plotted.
    show_fit_lines : tuple of 3 Booleans (optional)
        Specifies whether lines of fit should be plotted for the respective plots (SKR, QBER, attempts per success).
    skr_ylim : tuple of 2 floats (optional)
        Sets the limits for y-axis in the Secret Key Rate plot.
        When None, no limits are specified (default).
    qber_ylim : tuple of 2 floats (optional)
        Sets the limits for y-axis in the QBER plot.
        When None, no limits are specified (default).
    att_ylim : tuple of 2 floats (optional)
        Sets the limits for y-axis in the attempts per success plot.
        When None, no limits are specified (default).
    normalization : float (optional)
        Normalization constant to calculate the normalized secret key rate.
    convert_to_per_second : Boolean (optional)
        Whether to convert the unit of SKR from bits per attempt into bits per second.
    save_formats : String or tuple of Strings
        Defines the file formats in which the resulting plot should be saved.

    Returns
    -------
    plt : `matplotlib.pyplot`
        Object with resulting plot.
    """
    df_sk = pandas.read_csv(filename)
    df_sk = df_sk.sort_values(by=[scan_param_name])

    # fit
    skr_fit, norm_fit, qber_x_fit, qber_z_fit = fit_skr(df_sk, scan_param_name=scan_param_name,
                                                        normalization=normalization)

    skr_error = [df_sk.sk_rate - df_sk.sk_rate_lower_bound, df_sk.sk_rate_upper_bound - df_sk.sk_rate]
    if normalization is not None:
        norm_skr_error = [df_sk.norm_sk_rate - df_sk.norm_sk_rate_lower_bound,
                          df_sk.norm_sk_rate_upper_bound - df_sk.norm_sk_rate]

    # Create plots
    num_plots = plot_skr_qber_att.count(True)
    _set_font_sizes()
    fig, ax = plt.subplots(1, num_plots, figsize=(6 * num_plots, 5))

    if plot_skr_qber_att[0]:
        # Get axis for SKR plot
        skr_ax = ax if num_plots == 1 else ax[0]

        # Plot secret key rate without normalization
        if normalization is None:

            if convert_to_per_second:
                source_frequency = int(df_sk.source_frequency[0])
                df_sk.sk_rate *= source_frequency
                df_sk.sk_rate_lower_bound *= source_frequency
                df_sk.sk_rate_upper_bound *= source_frequency
                skr_fit_converted = []
                for val in skr_fit:
                    skr_fit_converted.append(val * source_frequency)
                skr_fit = skr_fit_converted
                print("Converted secret key rate from bits per attempt to bits per second.")

                # Save fit data in separate file
                # fit_output = pandas.DataFrame(data={
                #     "length": df_sk.length,
                #     "skr_fit": skr_fit_converted,
                #     "skr_lower": df_sk.sk_rate - df_sk.sk_rate_lower_bound,
                #     "skr_upper": df_sk.sk_rate_upper_bound - df_sk.sk_rate})
                # fit_output.to_csv("fit_output.csv", index=False)

            if shaded:
                # Plot with shaded area
                df_sk.plot(x=scan_param_name, y="sk_rate", yerr=skr_error, kind="scatter", color="xkcd:orange",
                           ax=skr_ax, logy=True, grid=True)

                # Plot normalized SKR as well
                # df_sk.plot(x=scan_param_name, y="norm_sk_rate", kind="scatter", color="blue", ax=skr_ax,
                # logy=True, grid=True)

                # skr_ax.plot(df_sk.get(scan_param_name), norm_fit, 'b')

                skr_ax.fill_between(df_sk.length, df_sk.sk_rate_lower_bound, df_sk.sk_rate_upper_bound, alpha=0.2,
                                    edgecolor="xkcd:orange", facecolor="xkcd:orange", linewidth=4, antialiased=True)

            else:
                df_sk.plot(x=scan_param_name, y="sk_rate", yerr=skr_error, kind="scatter",
                           color="xkcd:orange", ax=skr_ax, logy=False, grid=True)

            if show_fit_lines[0]:
                skr_ax.plot(df_sk.get(scan_param_name), skr_fit, 'r')

            skr_ax.set_ylabel("Secret key rate [bits/s]" if convert_to_per_second else "Secret key rate [bits/att.]")
            if skr_legend is not None:
                skr_ax.legend(skr_legend)

        # Plot secret key rate with normalization
        else:
            df_sk.plot(x=scan_param_name, y="norm_sk_rate", yerr=norm_skr_error, capsize=4, kind="line", color="red",
                       ax=skr_ax, logy=True)
            # Plot capacity bounds
            df_sk.plot(x=scan_param_name, y="sk_capacity", kind="line", color="blue", ax=skr_ax, logy=True)
            df_sk.plot(x=scan_param_name, y="tgw_bound", kind="line", color="green", ax=skr_ax, logy=True)

            skr_ax.set_ylabel("Normalized Secret-Key Rate [bits/attempt]")
            skr_ax.legend(["Capacity (blue)", "TGW (green)", "Normalized Rate (red)"])

        if skr_ylim is not None:
            skr_ax.set_ylim(skr_ylim)

        skr_ax.set_xlabel(scan_param_label)

    # Plot QBER (with fit)
    if plot_skr_qber_att[1]:
        # Get axis for QBER plot
        qber_ax = ax if num_plots == 1 else ax[plot_skr_qber_att[:-1].count(True) - 1]

        df_sk.plot(x=scan_param_name, y="Qber_x", yerr="Qber_x_error", kind="scatter", color="red", ax=qber_ax,
                   logy=False, grid=True)
        df_sk.plot(x=scan_param_name, y="Qber_z", yerr="Qber_z_error", kind="scatter", color="blue", ax=qber_ax,
                   logy=False, grid=True)

        if show_fit_lines[1]:
            qber_ax.plot(df_sk.get(scan_param_name), qber_z_fit, 'b')
            qber_ax.plot(df_sk.get(scan_param_name), qber_x_fit, 'r')
            qber_ax.legend(["Z basis (fit)", "X basis (fit)", "X basis", "Z basis"])
        else:
            qber_ax.legend(["X basis", "Z basis"])

        qber_ax.set_ylabel("QBER")

        if qber_ylim is not None:
            qber_ax.set_ylim(qber_ylim)

        qber_ax.set_xlabel(scan_param_label)

    # Plot attempts per success
    if plot_skr_qber_att[2]:
        # Get axis for attempts per success plot
        att_ax = ax if num_plots == 1 else ax[plot_skr_qber_att.count(True) - 1]

        df_sk.plot(x=scan_param_name, y="attempts_per_success", kind="scatter", color='green',
                   yerr="attempts_per_success_error", ax=att_ax, legend=True, logy=True, grid=True)

        if show_fit_lines[2]:
            fit = np.poly1d(np.polyfit(df_sk.get(scan_param_name), df_sk.attempts_per_success, 1))
            att_fit = []
            for k in range(len(df_sk.get(scan_param_name))):
                att_fit.append(fit(df_sk.get(scan_param_name)[k]))
            att_ax.plot(df_sk.get(scan_param_name), att_fit, 'b')

        att_ax.set_ylabel("Avg. num. of att./succ.")

        if att_ylim is not None:
            att_ax.set_ylim(att_ylim)

        att_ax.set_xlabel(scan_param_label)

    # Save figure
    save_filename = save_filename if save_filename is not None else "plot_qkd_data_" + filename.split("/")[-1][:-4]
    if type(save_formats) is str:
        fig.savefig(save_filename + save_formats, bbox_inches="tight")
    else:
        for fileformat in save_formats:
            fig.savefig(save_filename + fileformat, bbox_inches="tight")

    plt.show()
    return plt


def plot_fidelity_rate(filename, scan_param_name, scan_param_label, save_filename=None, plot_fid_rate=(True, True),
                       show_fit_lines=(True, True)):
    """Read a combined container with processed data and plots fidelity and number of attempts over the
    specified/varied parameter.

    Parameters
    ----------
    filename: str
        Filename of a csv file containing the processed data and the varied parameter. This .csv file can
        be generated with :meth:`process_fidelity_data` in `repchain_data_process.py`. The .csv file has to have the
        following columns: scan_param_name, 'fidelity' and 'fidelity_error' (if plotting fidelity),
        'attempts_per_success' and 'attempts_per_success_error' (if plotting rate).
    scan_param_name: str
        Name of parameter that was varied over and should be plotted on the x-axis.
    scan_param_label : str
        Label (name and unit) of the parameter that was varied and should be plotted on the x-axis.
        For example if length was the varied parameter, its label would be `Total Length [km]`.
    save_filename : str (optional)
        Name of the file the figure should be saved as.
        Default is None which creates a name for the saved plot using the name of the input file (filename).
    plot_fid_rate : tuple of 2 Booleans (optional)
        Specifies which plots should be plotted (fidelity, rate).
        By default, both plots are plotted.
    show_fit_lines : tuple of 2 Booleans (optional)
        Specifies whether lines of fit should be plotted for the respective plots (fidelity, rate).

    Returns
    -------
    plt : `matplotlib.pyplot`
        Object with resulting plot.
    """
    df_fid = pandas.read_csv(filename)

    # Create plots
    num_plots = plot_fid_rate.count(True)
    _set_font_sizes()
    fig, ax = plt.subplots(1, num_plots, figsize=(6 * num_plots, 5))

    # Plot fidelity
    if plot_fid_rate[0]:
        # Get axis for fidelity graph
        fid_ax = ax if num_plots == 1 else ax[0]

        df_fid.plot(x=scan_param_name, y="fidelity", yerr="fidelity_error", kind="scatter", color="blue", ax=fid_ax,
                    logy=True, grid=True)

        if show_fit_lines[0]:
            fit = np.poly1d(np.polyfit(df_fid.get(scan_param_name), df_fid.fidelity, 1))
            fid_fit = []
            for k in range(len(df_fid.get(scan_param_name))):
                fid_fit.append(fit(df_fid.get(scan_param_name)[k]))
            fid_ax.plot(df_fid.get(scan_param_name), fid_fit, 'b')

        fid_ax.set_ylabel("Average Fidelity")

        fid_ax.set_xlabel(scan_param_label)

    # Plot rate
    if plot_fid_rate[1]:
        # Get axis for rate graph
        rate_ax = ax if num_plots == 1 else ax[1]

        df_fid.plot(x=scan_param_name, y="attempts_per_success", kind="scatter", color="red",
                    yerr="attempts_per_success_error", ax=rate_ax, legend=False, grid=True)

        if show_fit_lines[1]:
            fit = np.poly1d(np.polyfit(df_fid.get(scan_param_name), df_fid.attempts_per_success, 1))
            rate_fit = []
            for k in range(len(df_fid.get(scan_param_name))):
                rate_fit.append(fit(df_fid.get(scan_param_name)[k]))
            rate_ax.plot(df_fid.get(scan_param_name), rate_fit, 'r')

        rate_ax.set_ylabel("Average number of attempts")

        rate_ax.set_xlabel(scan_param_label)

    save_filename = save_filename if save_filename is not None else "plot_fidelity_" + filename.split("/")[-1][:-4]
    plt.savefig(save_filename)

    plt.show()
    return plt


def plot_multiple_skr(raw_data_dir=".", convert_to_per_second=False, show_fit_line=True, ylim=None):
    """Plot Secret Key Rate for different QKD experiments in one plot.

    Parameters
    ----------
    raw_data_dir : str (optional)
        Directory with data that should be plotted. The .csv files with data to be plotted should contain columns
        'length', 'sk_rate', 'sk_rate_lower_bound', 'sk_rate_upper_bound', 'Qber_x', 'Qber_z', and
        'attempts_per_success'.
    convert_to_per_second : Boolean (optional)
        Whether to convert the unit of SKR from bits per attempt into bits per second.
    show_fit_line : Boolean (optional)
        Whether to show the lines of fit for plotted secret key rates.
    ylim : tuple of 2 floats (optional)
        Sets the limits for y-axis in the plot.
        When None, no limits are specified (default).

    Returns
    -------
    plt : `matplotlib.pyplot`
        Object with resulting plot.
    """

    if not os.path.exists(raw_data_dir):
        raise NotADirectoryError("No raw_data directory found!")

    legend = []
    _set_font_sizes()
    fig, ax = plt.subplots(1, 1)

    for i, filename in enumerate(os.listdir(raw_data_dir)):
        if filename[-3:] == "csv":
            legend.append(filename[:-4])

            # Read out csv files
            csv_data = pandas.read_csv(raw_data_dir + "/" + filename)

            skr_fit, norm_fit, qber_x_fit, qber_z_fit = fit_skr(csv_data, scan_param_name="length")
            skr_error = [csv_data.sk_rate - csv_data.sk_rate_lower_bound,
                         csv_data.sk_rate_upper_bound - csv_data.sk_rate]

            # Convert skr from [bits/attempt] to [bits/s]
            if convert_to_per_second:
                source_frequency = int(csv_data.source_frequency[0])
                csv_data.sk_rate *= source_frequency
                csv_data.sk_rate_lower_bound *= source_frequency
                csv_data.sk_rate_upper_bound *= source_frequency
                skr_fit_converted = []
                for val in skr_fit:
                    skr_fit_converted.append(val * source_frequency)

                if show_fit_line:
                    ax.plot(csv_data.length, skr_fit_converted, "C" + str(i))
                    legend.append(filename[:-4] + " (fit)")

            ax.errorbar(csv_data.length, csv_data.sk_rate, yerr=skr_error, color="C" + str(i), alpha=0.5, ls='')
            ax.plot(csv_data.length, csv_data.sk_rate, "o")

            if show_fit_line and not convert_to_per_second:
                ax.plot(csv_data.length, skr_fit, "C" + str(i))
                legend.append(filename[:-4] + " (fit)")

    ax.set_xlabel("Total Length [km]")
    ax.legend(legend, frameon=False)

    ax.set_ylabel("Secret Key Rate [bits/s]" if convert_to_per_second else "Secret Key Rate [bits/att.]")
    if ylim is not None:
        ax.set_ylim(ylim)
    ax.set_yscale("log")

    plt.show()
    return plt


def plot_multiple_fidelity(raw_data_dir=".", show_fit_line=True, ylim=None):
    """Plot Fidelity for different QKD experiments in one plot.

    Parameters
    ----------
    raw_data_dir : str (optional)
        Directory with data that should be plotted. The .csv files with data to be plotted should contain columns
        'length', 'fidelity', and 'fidelity_error'.
    show_fit_line : Boolean (optional)
        Whether to show the lines of fit for fidelities.
    ylim : tuple of 2 floats (optional)
        Sets the limits for y-axis in the plot.
        When None, no limits are specified (default).

    Returns
    -------
    plt : `matplotlib.pyplot`
        Object with resulting plot.
    """

    if not os.path.exists(raw_data_dir):
        raise NotADirectoryError("No raw_data directory found!")

    legend = []
    _set_font_sizes()
    fig, ax = plt.subplots(1, 1)

    for i, filename in enumerate(os.listdir(raw_data_dir)):
        if filename[-3:] == "csv":
            legend.append(filename[:-4])

            # Read out csv files
            csv_data = pandas.read_csv(raw_data_dir + "/" + filename)

            ax.errorbar(csv_data.length, csv_data.fidelity, yerr=csv_data.fidelity_error, color="C" + str(i), alpha=0.5, ls='')
            ax.plot(csv_data.length, csv_data.fidelity, "o")

            if show_fit_line:
                fit = np.poly1d(np.polyfit(csv_data.length, csv_data.fidelity, 1))
                fid_fit = []
                for k in range(len(csv_data.length)):
                    fid_fit.append(fit(csv_data.length[k]))
                ax.plot(csv_data.length, fid_fit, "C" + str(i))
                legend.append(filename[:-4] + " (fit)")

    ax.set_xlabel("Total Length [km]")
    ax.legend(legend, frameon=False)

    ax.set_ylabel("Average Fidelity")
    if ylim is not None:
        ax.set_ylim(ylim)
    ax.set_yscale("log")

    plt.show()
    return plt


def fit_skr(df_sk, scan_param_name, deg=1, normalization=None):
    """Fit polynomial of degree deg to QBER and calculate Secret key rate from fitted data.

    Parameters
    ----------
    df_sk : pandas.dataframe
        Dataframe containing the secret key rate data.
    scan_param_name : str
        Name of parameter that SKR should be fitted against.
    deg : int (optional)
        Degree of the polynomial to be fitted. Default is a linear fit.
    normalization : float (optional)
        Normalization constant to calculate the normalized secret key rate.

    Returns
    -------
    skr_fit : list
        List with values of secret key rate fitted for each datapoint of QBER.
    norm_fit : list
        List with values of normalized secret key rate fitted for each datapoint of QBER. If no normalization was
        specified this is an empty list.
    qber_x_fit : list
        List with values of fitted QBER in X basis.
    qber_z_fit : list
        List with values of fitted QBER in Z basis.
    """
    skr_fit = []
    norm_fit = []

    # numpy polynomial fit
    try:
        fit_x = np.poly1d(np.polyfit(df_sk.get(scan_param_name), df_sk.Qber_x, deg=deg))
        fit_z = np.poly1d(np.polyfit(df_sk.get(scan_param_name), df_sk.Qber_z, deg=deg))

        qber_x_fit = []
        qber_z_fit = []
        for k in range(len(df_sk.get(scan_param_name))):
            qber_x_fit.append(fit_x(df_sk.get(scan_param_name)[k]))
            qber_z_fit.append(fit_z(df_sk.get(scan_param_name)[k]))

    except AttributeError:
        fit_x = np.poly1d(np.polyfit(df_sk.spectral_modes, df_sk.Qber_x, deg=deg))
        fit_z = np.poly1d(np.polyfit(df_sk.spectral_modes, df_sk.Qber_z, deg=deg))

        qber_x_fit = []
        qber_z_fit = []
        for k in range(len(df_sk.spectral_modes)):
            qber_x_fit.append(fit_x(df_sk.spectral_modes[k]))
            qber_z_fit.append(fit_z(df_sk.spectral_modes[k]))

    for n in range(len(qber_x_fit)):
        secret_key_rate_fit, skr_min_fit, skr_max_fit, _ = _secret_key_rate(qber_x_fit[n], 0, qber_z_fit[n], 0,
                                                                            df_sk.attempts_per_success[n],
                                                                            df_sk.attempts_per_success_error[n])
        skr_fit.append(secret_key_rate_fit / df_sk.attempts_per_success[n])
        if normalization is not None:
            norm_fit.append(secret_key_rate_fit / (normalization * df_sk.attempts_per_success[n]))

    # if normalization is None:
    #    for k in range(len(df_sk.spectral_modes)):
    #        norm_fit.append(skr_fit[k] / (2 * df_sk.spectral_modes[k]))

    return skr_fit, norm_fit, qber_x_fit, qber_z_fit


def _set_font_sizes(small=14, medium=19, large=30, usetex=False):
    """Set font sizes of the plot.

    Parameters
    ----------
    small : float (optional)
        Size of default font, axes labels, ticks and legend.
    medium : float (optional)
        Fontsize of x and y labels.
    large : float (optional)
        Fontsize of figure title.
    usetex : bool (optional)
        Whether to use latex for all text output. Default is False.
    """

    plt.rc('font', size=small)          # controls default text sizes
    plt.rc('axes', titlesize=small)     # fontsize of the axes title
    plt.rc('axes', labelsize=medium)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=small)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=small)    # fontsize of the tick labels
    plt.rc('legend', fontsize=small)    # legend fontsize
    plt.rc('figure', titlesize=large)   # fontsize of the figure title
    plt.rc('text', usetex=usetex)         # use LaTeX for text output


if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument('-f', '--filename', required=False, type=str,
                        help="Name of the container file.")
    parser.add_argument('-p', '--parameter', required=False, type=str,
                        help="Name of the varied parameter.")
    args = parser.parse_args()

    # plot_qkd_data(filename="output.csv", scan_param_name="length", scan_param_label="Total Length [km]")
