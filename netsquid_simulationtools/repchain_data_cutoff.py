import warnings

from netsquid_simulationtools.repchain_dataframe_holder import RepchainDataFrameHolder
from netsquid_simulationtools.repchain_data_process import process_container_qkd
from netsquid_simulationtools.repchain_data_combine import combine_data
from copy import deepcopy
import pickle
import time
import os
import matplotlib.pyplot as plt


def implement_cutoff_rounds(repchain_dataframe_holder, max_num_rounds, use_default_method=True):
    """Function which changes a repchain_dataframe_holder such that it is as if it were performed using a cutoff time.

    Parameters
    ----------
    repchain_dataframe_holder : :object:`~.repchain_dataframe_holder.RepchainDataFrameHolder`
        Container holding all the results of a simulation without cutoff and all its parameters.
    max_num_rounds : int
        Maximum number of rounds entanglement is stored in memory.
    use_default_method : bool
        Determines whether default or nondefault implementation of the function should be used.
        The nondefault implementation may be faster under some circumstances.

    Returns
    -------
    :object:`~.repchain_dataframe_holder.RepchainDataFrameHolder`
        Container holding all the results of the simulation as if it were performed with a cutoff time.

    Note
    ----
    Currently only works for a single sequential quantum repeater.
    The input repchain_dataframe_holder must have "rounds_between_alice_and_bob" as a varied parameter.
    There is both a default and nondefault implementation of this function. While they should have the same result,
    one may be more efficient than the other in some circumstances. Performance of both implementations has not
    yet been studied.

    Example
    -------

    >>> data = {"outcome_A": [0, 1, 0], "basis_A": ["Z", "Z", "X"],
    >>>         "outcome_B": [0, 1, 0], "basis_B": ["Z", "Z", "X"],
    >>>         "number_of_rounds": [100, 100, 100],
    >>>         "midpoint_outcome_0": [0, 0, 0], "midpoint_outcome_1": [0, 0, 0], "swap_outcome_0": [0, 0, 0],
    >>>         "rounds_between_alice_and_bob": [10, 30, 15]}
    >>>
    >>> repchain_dataframe_holder = RepchainDataFrameHolder(data)
    >>> repchain_dataframe_holder_with_cutoff = implement_cutoff_rounds(repchain_dataframe_holder, 20)
    >>>
    >>> print(repchain_dataframe_holder_with_cutoff)
    >>> print(repchain_dataframe_holder_with_cutoff.baseline_parameters)
    >>>

     output:

        outcome_A basis_A  outcome_B basis_B  number_of_rounds  midpoint_outcome_0  midpoint_outcome_1  swap_outcome_0
     0          0       Z          0       Z             100.0                   0                   0               0
     2          0       X          0       X             190.0                   0                   0               0
        rounds_between_alice_and_bob
     0          10
     2          15

    {'cutoff_round': 20}

    If there were a cutoff of 20 rounds, during what is the second entanglement distribution (row)
    in the original data, entanglement would have been discarded 10 rounds before entanglement was successfully swapped.
    At this point, the setup would have to start all over again.
    To represent this in the data when implementing a cutoff time, it is assumed that at this point we are at the
    beginning of what in in the original data is the third entanglement distribution.
    In the data with cutoff time, there are now only two entanglement distributions,
    the second of which is basically the third of the orginal data,
    but it also includes rounds that were "wasted" by discarding entanglement.

    """

    def _implement_cutoff_rounds_default(repchain_dataframe_holder, max_num_rounds):
        """Function which changes a repchain_dataframe_holder such that it is as if there was a cutoff time.

        Parameters
        ----------
        repchain_dataframe_holder: ~.repchain_dataframe_holder.RepchainDataFrameHolder
            Container holding all the results of a simulation without cutoff and all its parameters.
        max_num_rounds: int
            Maximum number of rounds entanglement is stored in memory.

        Returns
        -------
        :object:`~.repchain_dataframe_holder.RepchainDataFrameHolder`
            Container holding all the results of the simulation as if it were performed with a cutoff time.

        Note
        ----
        Currently only works for a single sequential quantum repeater.
        The input repchain_dataframe_holder must have "rounds_between_alice_and_bob" as a varied parameter.
        Default function used to implement :func:`~.repchain_data_cutoff.implement_cutoff_rounds`

        """
        df = deepcopy(repchain_dataframe_holder.dataframe)

        # remove results to make sure last one does not exceed cutoff time (helps in next part)
        to_be_removed = []
        for index in reversed(df.index.values.tolist()):
            if df.loc[index, "rounds_between_alice_and_bob"] > max_num_rounds:
                to_be_removed.append(index)
            else:
                break
        df = df.drop(to_be_removed)

        # determine which results exceed cutoff and see how much "extra" rounds this means for next result
        exceeding = df["rounds_between_alice_and_bob"] > max_num_rounds
        df.loc[exceeding, "carry_over"] = df["number_of_rounds"] - df["rounds_between_alice_and_bob"] + max_num_rounds

        # add "extra" rounds (carry over) to the next result
        carry_over = 0
        for row in zip(df[exceeding].index, df.loc[exceeding, "carry_over"]):
            next_index = row[0] + 1
            carry_over += row[1]
            if df.loc[next_index, "carry_over"] != df.loc[next_index, "carry_over"]:
                df.loc[next_index, "number_of_rounds"] += carry_over
                carry_over = 0

        # filter
        df = df[df["rounds_between_alice_and_bob"] <= max_num_rounds]
        df = df.drop("carry_over", 1)

        # rebuild repchain_dataframe_holder and add "cutoff_round" as baseline parameter
        new_baseline_parameters = deepcopy(repchain_dataframe_holder.baseline_parameters)
        new_baseline_parameters.update({"cutoff_round": max_num_rounds})
        old_description = deepcopy(repchain_dataframe_holder.description)
        if old_description is None:
            old_description = ""
        new_description = old_description + "\n \n implemented cutoff in postprocessing"

        result = RepchainDataFrameHolder(number_of_nodes=repchain_dataframe_holder.number_of_nodes,
                                         baseline_parameters=new_baseline_parameters,
                                         description=new_description,
                                         data=df
                                         )

        return result

    def _implement_cutoff_rounds_nondefault(repchain_dataframe_holder, max_num_rounds):
        """Function which changes a repchain_dataframe_holder such that it is as if there were cutoff time.

        Parameters
        ----------
        repchain_dataframe_holder: :object:`~.repchain_dataframe_holder.RepchainDataFrameHolder`
            Container holding all the results of a simulation without cutoff and all its parameters.
        max_num_rounds: int
            Maximum number of rounds entanglement is stored in memory.

        Returns
        -------
        :object:`~.repchain_dataframe_holder.RepchainDataFrameHolder`
            Container holding all the results of the simulation as if it were performed with a cutoff time.

        Note
        ----
        Currently only works for a single sequential quantum repeater.
        The input repchain_dataframe_holder must have "rounds_between_alice_and_bob" as a varied parameter.
        Default function used to implement :func:`~.repchain_data_cutoff.implement_cutoff_rounds`

        """

        df = deepcopy(repchain_dataframe_holder.dataframe)

        # first, we identify which rows of the dataframe contain a result that would have hit the cut-off time.

        # filtered dataframe
        filtered_df = df[df["rounds_between_alice_and_bob"] > max_num_rounds]

        # secondly, starting from the last result, we add the number of cutoff rounds
        # to the number of attempts to the result after a result that hit the cutoff,
        # and remove the result that hit the cutoff.
        # This procedure ensures that while results that were cut off are filtered out,
        # the counting of the number of attempts remains correct.
        # Note that it is important to start from the bottom because consecutive results could be cut off.

        number_of_rounds_integer_position = df.columns.get_loc("number_of_rounds")
        for index in reversed(filtered_df.index.values.tolist()):
            row_integer_position = df.index.get_loc(index)
            row = df.iloc[row_integer_position]
            if row_integer_position + 1 != df.shape[0]:
                rounds_before_state_in_memory = row["number_of_rounds"] - row["rounds_between_alice_and_bob"]
                df.iat[row_integer_position + 1, number_of_rounds_integer_position] += \
                    rounds_before_state_in_memory + max_num_rounds

            df = df.drop(index)

        new_baseline_parameters = deepcopy(repchain_dataframe_holder.baseline_parameters)
        new_baseline_parameters.update({"cutoff_round": max_num_rounds})
        old_description = deepcopy(repchain_dataframe_holder.description)
        if old_description is None:
            old_description = ""
        new_description = old_description + "\n \n implemented cutoff in postprocessing"

        result = RepchainDataFrameHolder(number_of_nodes=repchain_dataframe_holder.number_of_nodes,
                                         baseline_parameters=new_baseline_parameters,
                                         description=new_description,
                                         data=df
                                         )

        return result

    if use_default_method:
        warnings.warn('Choosing default of two methods for updating RepchainDataframeHolder with cut-off criterion. '
                      'The nondefault method might be faster.')
        return _implement_cutoff_rounds_default(repchain_dataframe_holder=repchain_dataframe_holder,
                                                max_num_rounds=max_num_rounds)
    else:
        warnings.warn('Choosing nondefault of two methods for updating RepchainDataframeHolder with cut-off criterion. '
                      'The default method might be faster.')
        return _implement_cutoff_rounds_nondefault(repchain_dataframe_holder=repchain_dataframe_holder,
                                                   max_num_rounds=max_num_rounds)


def scan_cutoff(cutoff_round_min, cutoff_round_max, stepsize, use_default_method=True,
                target_dir="cutoff", filename="combined_state_data.pickle"):
    """Change simulation results as if a cutoff time had been used, for a range of different cutoff times.

    For a number of different values of the cutoff_round, a repchain_dataframe_holder is modified such that
    it is as if the experiment would have been performed using a cutoff time corresponding to cutoff_round number
    of rounds of entanglement generation. Each resulting repchain_dataframe_holder is saved individually.

    Parameters
    ----------
    cutoff_round_min: int
        lower bound on cutoffs to implement in number of rounds
    cutoff_round_max: int
        upper bound on cutoffs to implement in number of rounds
    stepsize: int
        difference in number of rounds between different cutoff to implement
    use_default_method : bool
        determines whether default or nondefault implementation of
        :func:`~.repchain_data_cutoff.implement_cutoff_rounds` should be used.
        The nondefault implementation may be faster under some circumstances.
    target_dir: str (optional)
        name of directory to store results
    filename: str (optional)
        name of file holding pickled :object:`~.repchain_dataframe_holder.RepchainDataFrameHolder`
        to implement cutoff on

    Note
    ----
    Output :object:`~.repchain_dataframe_holder.RepchainDataFrameHolder` objects are pickled and saved in the target_dir
    with filenames "cutoff=*", where * = cutoff in number of rounds.
    Uses :func:`~.repchain_data_cutoff.implement_cutoff_rounds` function
    (but is more efficient than just calling the function for each cutoff time individually,
    because results obtained with a larger cutoff time are reused when calculating results for a shorter cutoff time).
    Currently only works for a single sequential quantum repeater.
    The input repchain_dataframe_holder must have "rounds_between_alice_and_bob" as a varied parameter.

    """
    start_time = time.time()

    if not os.path.isfile(filename):
        raise FileNotFoundError("File not found: {}".format(filename))
    repchain_dataframe_holder = pickle.load(open(filename, "rb"))

    # directory to save cutoff results
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)

    number_of_cutoffs = 0

    data = implement_cutoff_rounds(repchain_dataframe_holder, cutoff_round_max, use_default_method=use_default_method)
    pickle.dump(data, open("{}/cutoff={}.pickle".format(target_dir, cutoff_round_max), "wb"))

    for cutoff in reversed(range(cutoff_round_min, cutoff_round_max, stepsize)):

        data = implement_cutoff_rounds(data, cutoff, use_default_method=use_default_method)
        pickle.dump(data, open("{}/cutoff={}.pickle".format(target_dir, cutoff), "wb"))
        number_of_cutoffs += 1

    total_time = time.time() - start_time

    print("\n\nImplemented {} different cutoff times on {} results in {} seconds.\n"
          .format(number_of_cutoffs, repchain_dataframe_holder.number_of_results, total_time))


def process_qkd_cutoff(directory="cutoff"):
    """Combine results of scan_cutoff and perform QKD processing (calculating e.g. SKR as function of cutoff)

    Parameters
    ----------
    directory: str (optional)
        name of directory containing pickled :object:`~.repchain_dataframe_holder.RepchainDataFrameHolder`s
        with different cutoffs.

    Note
    ----
    Stores pickled combined :object:`~.repchain_dataframe_holder.RepchainDataFrameHolder` as
    "combined_cutoff_data.pickle".
    The output data of QKD processing is saved as "cutoff_processed.csv".

    """
    if not os.path.exists(directory):
        raise NotADirectoryError("Directory not found: {}".format(directory))

    combine_data(raw_data_dir=directory, suffix=".pickle", output="combined_cutoff_data.pickle")

    combined_data = pickle.load(open("combined_cutoff_data.pickle", "rb"))
    results = process_container_qkd(combined_data)

    results.to_csv("cutoff_processed.csv", index=False)


def show_cutoff_histo(filename="combined_state_data.pickle"):
    """Show a histogram of how often each amount of waiting time occurs in a repchain_dataframe_holder.

    Parameters
    ----------
    filename: str
        name of file holding pickled :object:`~.repchain_dataframe_holder.RepchainDataFrameHolder`

    Note
    ----
    Only works for single sequential quantum repeater.
    The input repchain_dataframe_holder must have "rounds_between_alice_and_bob" as a varied parameter.

    """
    data = pickle.load(open(filename, "rb"))
    data.dataframe.hist(column="rounds_between_alice_and_bob", bins=100)
    plt.show()
