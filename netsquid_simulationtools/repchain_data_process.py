import itertools
from copy import deepcopy

import pandas
import numpy as np
import time

from netsquid_simulationtools.repchain_data_combine import combine_data
from netsquid_simulationtools.repchain_data_functions import qber, secret_key_rate_from_outcomes, end_to_end_fidelity
from netsquid_simulationtools.repchain_data_plot import plot_qkd_data, plot_fidelity_rate


def process_qkd_data(raw_data_dir="raw_data", suffix="_measurement.pickle", output="processed_qkd_data.pickle",
                     csv_output_files=("output.csv", "csv_output.csv"), plot_processed_data=True):
    """Read different RepchainDataFrameHolder files with a common suffix to calculate secret-key-rate and other figures
    of merit.

    Parameters
    ----------
    raw_data_dir : str (optional)
        Directory containing the data (pickle files) to be processed.
    suffix : str (optional)
        Common ending of the files to be combined.
    output : str (optional)
        filename of combined repchain_dataframe_holder.
    csv_output_files : tuple of strings (optional)
        Filenames of csv output for smart-stopos.
    plot_processed_data : Boolean (optional)
        If True, plots the processed qkd data (default).
        If False, nothing is plotted.
    """
    start_time = time.time()

    combined_data = combine_data(raw_data_dir=raw_data_dir, suffix=suffix, output=output, save_output_to_file=True)

    processed_data = process_container_qkd(combined_data)

    # sort data by first scan_param
    processed_data.sort_values(by=combined_data.varied_parameters[0], inplace=True)

    # save processed data
    processed_data.to_csv(csv_output_files[0], index=False)

    # save to csv_output.csv for stopos
    csv_output = processed_data[["norm_sk_rate"] + combined_data.varied_parameters]
    csv_output.to_csv(csv_output_files[1], index=False)

    runtime = time.time() - start_time

    print("processed files in {} with suffix {} in {} seconds\n"
          .format(raw_data_dir, suffix, runtime))

    if plot_processed_data:
        plot_qkd_data(filename=csv_output_files[0], scan_param_name=combined_data.varied_parameters[0],
                      scan_param_label=combined_data.varied_parameters[0])


def process_container_qkd(container):
    """Process single container holding raw QKD data and returns a dataframe with processed data (secret-key rate).

    Parameters
    ----------
    container : ~.repchain_dataframe_holder.RepchainDataFrameHolder
        Container holding all the results of the simulation and all its parameters.

    Return
    ------
    data : pandas.DataFrame
        Dataframe containing secret-key rate, qber (z and x) and number of attempts as function of
        varied parameter (including error bounds).
    """
    # access data in container
    base_params = container.baseline_parameters
    varied_params = container.varied_parameters
    if "number_of_successes" in varied_params:
        varied_params.remove("number_of_successes")

    varied_params_unique_values = []
    for varied_param in varied_params:
        varied_params_unique_values.append(container.dataframe[varied_param].unique().tolist())

    data = pandas.DataFrame()
    for values in itertools.product(*varied_params_unique_values):

        sim_params = deepcopy(base_params)
        df = container.dataframe
        varied_params_with_values = {}
        for (varied_param, value) in zip(varied_params, values):
            sim_params.update({varied_param: value})
            varied_params_with_values.update({varied_param: value})
            df = df[df[varied_param] == value]
        df.drop(columns=varied_params)

        if df.empty:
            continue

        # specify normalization of secret key rate : norm_skr = skr [bits/attempt] / normalization
        try:
            normalization = sim_params["spectral_modes"] * 2
        except KeyError:
            normalization = 2

        number_of_successes = len(df.index)
        try:
            number_of_rounds = df.sum(axis=0)["number_of_rounds"]
        except KeyError:
            number_of_rounds = df.sum(axis=0)["number_of_attempts"]

        current_length = sim_params["length"]
        attenuation = sim_params["attenuation"] if "attenuation" in sim_params else 0.25
        if "attenuation" not in sim_params:
            print("NOTE: capacity is based on default attenuation of 0.25 db/km, which may not correspond to "
                  "attenuation used in simulations.")

        # calculate secret-key capacity
        sk_capacity, tgw_bound = secret_key_capacity(length=current_length, attenuation=attenuation)

        # calculate qber in X and Z
        qber_z, qber_z_error = qber(df, "Z")
        qber_x, qber_x_error = qber(df, "X")

        # determine secret key rate in [bits/attempt]
        secret_key_rate, skr_min, skr_max, skr_error = secret_key_rate_from_outcomes(df)
        normalized_secret_key_rate = secret_key_rate / normalization
        norm_skr_min = skr_min / normalization
        norm_skr_max = skr_max / normalization
        norm_skr_error = skr_error / normalization

        ##############################

        # calculate average number of attempts needed for one success event
        attempt_per_success = number_of_rounds / number_of_successes
        # attempt_per_success_error = attempt_per_success / np.sqrt(number_of_rounds)
        attempt_per_success_error = df.std(axis=0)["number_of_rounds"] / np.sqrt(number_of_successes)

        # put all results in dictionary
        data_these_parameters_dict = {
            "norm_sk_rate": [normalized_secret_key_rate],
            "norm_sk_rate_upper_bound": [norm_skr_max],
            "norm_sk_rate_lower_bound": [norm_skr_min],
            "norm_sk_rate_error": [norm_skr_error],
            "sk_rate": [secret_key_rate],
            "sk_rate_upper_bound": [skr_max],
            "sk_rate_lower_bound": [skr_min],
            "sk_error": [skr_error],
            "Qber_z": [qber_z],
            "Qber_z_error": [qber_z_error],
            "Qber_x": [qber_x],
            "Qber_x_error": [qber_x_error],
            "number_of_successes": [number_of_successes],
            "attempts_per_success": [attempt_per_success],
            "attempts_per_success_error": [attempt_per_success_error],
            "sk_capacity": [sk_capacity],
            "tgw_bound": [tgw_bound],
        }

        data_these_parameters_dict.update(varied_params_with_values)
        data = data.append(pandas.DataFrame(data_these_parameters_dict))

    return data


def process_fidelity_data(raw_data_dir="raw_data", suffix="_measurement.pickle",
                          output="processed_fidelity_data.pickle", csv_output="fidelity_output.csv",
                          plot_processed_data=True):
    """Read different RepchainDataFrameHolder files with a common suffix to calculate fidelity and other figures
       of merit.

       Parameters
       ----------
       raw_data_dir : str (optional)
           Directory containing the data (pickle files) to be processed.
       suffix : str (optional)
           Common ending of the files to be combined.
       output : str (optional)
           filename of combined repchain_dataframe_holder.
       csv_output : str (optional)
           Filename of csv output.
       plot_processed_data : Boolean (optional)
           If True, plots the processed qkd data (default).
           If False, nothing is plotted.
    """
    start_time = time.time()

    combined_data = combine_data(raw_data_dir=raw_data_dir, suffix=suffix, output=output, save_output_to_file=True)

    processed_data = process_fidelity_container(combined_data)

    # sort data by first scan_param
    processed_data.sort_values(by=combined_data.varied_parameters[0], inplace=True)

    # save processed data
    processed_data.to_csv(csv_output, index=False)

    runtime = time.time() - start_time

    print("processed files in {} with suffix {} in {} seconds\n"
          .format(raw_data_dir, suffix, runtime))

    if plot_processed_data:
        plot_fidelity_rate(filename=csv_output, scan_param_name=combined_data.varied_parameters[0],
                           scan_param_label=combined_data.varied_parameters[0])


def process_fidelity_container(container):
    """Process single container holding raw QKD data and returns a dataframe with processed data (fidelity).

    Parameters
    ----------
    container : ~.repchain_dataframe_holder.RepchainDataFrameHolder
        Container holding all the results of the simulation and all its parameters.

    Return
    ------
    data : pandas.DataFrame
        Dataframe containing fidelity and number of attempts as function of varied parameter (including error bounds).
    """
    # access data in container
    base_params = container.baseline_parameters
    varied_params = container.varied_parameters
    if "number_of_successes" in varied_params:
        varied_params.remove("number_of_successes")

    varied_params_unique_values = []
    for varied_param in varied_params:
        varied_params_unique_values.append(container.dataframe[varied_param].unique().tolist())

    data = pandas.DataFrame()
    for values in itertools.product(*varied_params_unique_values):

        sim_params = deepcopy(base_params)
        df = container.dataframe
        varied_params_with_values = {}
        for (varied_param, value) in zip(varied_params, values):
            sim_params.update({varied_param: value})
            varied_params_with_values.update({varied_param: value})
            df = df[df[varied_param] == value]
        df.drop(columns=varied_params)

        if df.empty:
            continue

        number_of_successes = len(df.index)
        try:
            number_of_rounds = df.sum(axis=0)["number_of_rounds"]
        except KeyError:
            number_of_rounds = df.sum(axis=0)["number_of_attempts"]

        # calculate average number of attempts needed for one success event
        attempt_per_success = number_of_rounds / number_of_successes
        # attempt_per_success_error = attempt_per_success / np.sqrt(number_of_rounds)
        attempt_per_success_error = df.std(axis=0)["number_of_rounds"] / np.sqrt(number_of_successes)

        # calculate fidelity and its error
        fidelity, fidelity_error = end_to_end_fidelity(df)

        # put all results in dictionary
        data_these_parameters_dict = {
            "fidelity": [fidelity],
            "fidelity_error": [fidelity_error],
            "attempts_per_success": [attempt_per_success],
            "attempts_per_success_error": [attempt_per_success_error],
        }

        data_these_parameters_dict.update(varied_params_with_values)
        data = data.append(pandas.DataFrame(data_these_parameters_dict))

    return data


def secret_key_capacity(length, attenuation=0.25):
    """Calculate secret-key-rate capacity (i.e. the maximum attainable for direct transmission) and the Takeoka-Guha-
    Wilde (TGW) bound.

    Parameters
    ----------
    length : float
        Distance between end nodes performing QKD.
    attenuation : float
        Fiber loss in dB/km.

    Return
    ------
    sk_capacity : float
        Secret-key-rate capacity for direct transmission.
    tgw_bound : float
        Takeoka-Guha-Wilde bound for direct transmission.

    """
    if length < 0:
        raise ValueError("Length cannot be negative.")
    transmissivity = 0.9999 if np.isclose(length, 0) else np.power(10, - attenuation * length / 10)
    sk_capacity = -np.log2(1 - transmissivity)  # capacity
    tgw_bound = np.log2((1 + transmissivity) / (1 - transmissivity))  # tgw

    return sk_capacity, tgw_bound


if __name__ == "__main__":

    process_qkd_data()
