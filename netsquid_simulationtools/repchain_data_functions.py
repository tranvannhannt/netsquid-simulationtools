"""Unified processing functions for repeater chain simulations

Note: For now only use with Bell states as source states.
"""

from statistics import mean, stdev
import numpy as np
from netsquid import BellIndex
from scipy import sparse
import netsquid.qubits.qubitapi as qapi
from netsquid.qubits.ketstates import b01, b11, b00, b10


def secret_key_rate_from_outcomes(dataframe):
    """ Function that computes the secret key rate and its standard deviation from the simulation data in the input
    DataFrame.

    This is done by calculating the Quantum Bit Error Rate (QBER) in X and Z basis and then taking:
    max(0., 1 - H(qber_x) - H(qber_z))

    where H(p) is the binary entropy function:
    H(p) = -p log(p) - (1-p) log(1-p)

    Parameters
    ----------
    dataframe : pandas.DataFrame
        Dataframe containing simulation data


    Return
    ------
    secret_key_rate : float
        Secret Key Rate in [bits/attempt]. Note: The User can than convert this into [bits/second] or [bits/channel_use]
    skr_min : float
        Minimal value of Secret Key Rate within the interval given by the Stdev of the Qber
    skr_max : float
        Maximal value of Secret Key Rate within the interval given by the Stdev of the Qber
    skr_error : float
        Symmetric error calculated from the standard deviations of the QBERs and the number of attempts/success.

    """
    # calculated qber in X,Z and their error (standard deviation of the mean)
    qber_x, qber_x_error = qber(dataframe, "X")
    qber_z, qber_z_error = qber(dataframe, "Z")

    # get total number of successful bits
    df = dataframe
    dataframe_z = df[(df.basis_A == df.basis_B) & (df.basis_A == "Z")]
    dataframe_x = df[(df.basis_A == df.basis_B) & (df.basis_A == "X")]
    successful_bits_generated = len(dataframe_z.index) + len(dataframe_x.index)

    if successful_bits_generated == 0:
        return 0, 0, 0, 0

    # get total number of attempts
    try:
        number_of_attempts = df["number_of_rounds"].tolist()
    except KeyError:
        number_of_attempts = df["number_of_attempts"].tolist()
    total_attempts = sum(number_of_attempts)

    attempts_per_success = total_attempts / successful_bits_generated
    attempts_per_success_error = stdev(number_of_attempts) / np.sqrt(total_attempts)

    secret_key_rate, skr_min, skr_max, skr_error = _secret_key_rate(qber_x, qber_x_error,
                                                                    qber_z, qber_z_error,
                                                                    attempts_per_success, attempts_per_success_error)

    return secret_key_rate, skr_min, skr_max, skr_error


def secret_key_rate_from_states():
    """ Function that should compute the secret key rate from the states saved in the input DataFrame.

    Return
    ------
    secret_key_rate : float
        Secret key rate calculated from the DataFrame.
    secret_key_error: float
        Standard deviation of the secret key rate

    """
    # calculate QBER in X and Z from density matrix
    # return _secret_key_rate(qber_x, 0, qber_z, 0)
    raise NotImplementedError("Not yet implemented.")


def end_to_end_fidelity(dataframe):
    """Function that computes the average end-to-end fidelity from the input dataframe.

    Note: The Dataframe has to contain the end-to-end states.

    Parameters
    ----------
    dataframe : pandas.DataFrame
        DataFrame containing simulation data (especially end-to-end states and midpoint outcomes).

    Returns
    -------
    avg_fidelity : float
        Average fidelity measured for the states in the DataFrame.
    fidelity_error : floar
        Error of the average fidelity for the DataFrame.

    """
    fidelities = []
    for index, row in dataframe.iterrows():
        if row["state"] is None:
            raise ValueError("Can't calculate fidelity without end-to-end state.")
        else:
            dm = row["state"]

        if sparse.issparse(dm):
            # convert sparse to dense
            dm = dm.toarray()

        # create qubits with state
        num_qubits = int(np.log2(dm.shape[0]))
        qubits = qapi.create_qubits(num_qubits=num_qubits)
        qapi.assign_qstate(qubits, dm)

        # get index from expected target state
        bell_index = _expected_target_state(row)
        # convert index to reference ket
        bell_index_to_ket = [b00, b01, b11, b10]
        reference_ket = bell_index_to_ket[bell_index]

        if dm.shape[0] == 256:
            # create relevant target states
            if bell_index == 1:
                # target state: (|01,00;00,01> + |00,01;01,00>)/sqrt(2)
                reference_ket = np.array([[0. + 0.j]]*256)
                reference_ket[65] = 1/np.sqrt(2.) + 0.j
                reference_ket[20] = 1/np.sqrt(2.) + 0.j
            elif bell_index == 2:
                # target state: (|01,00;00,01> - |00,01;01,00>)/sqrt(2)
                reference_ket = np.array([[0. + 0.j]]*256)
                reference_ket[65] = 1/np.sqrt(2.) + 0.j
                reference_ket[20] = - 1/np.sqrt(2.) + 0.j
            else:
                # discarding these events
                break
        elif dm.shape[0] == 16:
            # map kets to multi photon encoding
            zero = np.array([[0. + 0.j]]*16)
            zero[0] = reference_ket[0]
            zero[1] = reference_ket[1]
            zero[4] = reference_ket[2]
            zero[5] = reference_ket[3]
            reference_ket = zero

        # calculate fidelity
        f = qapi.fidelity(qubits, reference_ket, squared=True)

        # append
        fidelities.append(f)
    # calculate mean and std
    avg_fidelity = mean(fidelities)
    fidelity_error = stdev(fidelities) / np.sqrt(len(fidelities))

    return avg_fidelity, fidelity_error


def agreement_with_expected_outcome(dataframe, basis_a, basis_b=None):
    """ Function that checks whether the measured outcomes agree with the expected outcomes in the input DataFrame,
    for a given pair of basis choices.
    Or in other words: The function checks whether the measurement outcomes of Alice and Bob, for which we expect
    (anti-)correlation from the simulation data, are actually (anti-)correlated and assigns 0 for agreement and 1 for
    disagreement.

    Parameters
    ----------
    dataframe : pandas.DataFrame
        DataFrame containing simulation data
    basis_a: string
        Basis in which Alice measured. Should be in ["X", "Y", "Z"]
    basis_b: string , optional
        Basis in which Bob measured, if not specified will be set to the same basis as Alice by default.


    Return
    ------
        agreement_for_basis : list
            list of {0,1} for a given basis pair where 0, 1 correspond to 'no agreement' and 'agreement' respectively

        """
    if basis_b is None:
        basis_b = basis_a

    agreement_for_basis = []
    for index, row in dataframe.iterrows():
        if row["basis_A"] == basis_a and row["basis_B"] == basis_b:
            if _do_we_expect_correlation(row):
                if row["outcome_A"] == row["outcome_B"]:
                    agreement_for_basis.append(1)
                else:
                    agreement_for_basis.append(0)
            else:
                if row["outcome_A"] != row["outcome_B"]:
                    agreement_for_basis.append(1)
                else:
                    agreement_for_basis.append(0)

    return agreement_for_basis


def qber(dataframe, basis_a, basis_b=None, quantile=1):
    """
    Function that calculates the QBER for a specified basis (set) from the input DataFrame.

    Parameters
    ----------
    dataframe : pandas.DataFrame
        DataFrame containing simulation data
    basis_a : string
        Basis in which Alice measured. Should be in ["X", "Y", "Z"]
    basis_b : string , optional
        Basis in which Bob measured, if not specified will be set to the same basis as Alice by default.
    quantile : float , optional
        1 - alpha/2 quantile of a standard normal distribution corresponding to the target error rate alpha.
        For a 95% confidence level, the error alpha = 1 − 0.95 = 0.05 , so (1 − alpha/2) = 0.975 and quantile = 1.96.
        Default is quantile = 1


    Return
    ------
    qber : float
        QBER (quantum bit error rate)

    qber_error : float
        QBER error (standard deviation of the mean)

    """
    if basis_b is None:
        basis_b = basis_a

    agreement_list = agreement_with_expected_outcome(dataframe, basis_a, basis_b)
    if agreement_list:
        qber = 1 - mean(agreement_list)
        qber_error = quantile * stdev(agreement_list) / np.sqrt(len(agreement_list))
    else:
        # list is empty
        qber, qber_error = 0, 0

    return qber, qber_error


def _binary_entropy(p):
    """Calculate binary entropy.

    H(p) = -p log(p) - (1-p) log(1-p)

    Parameters
    ----------
    p : float
        Probability value to calculate binary entropy for.


    Return
    ------
    float
        Binary entropy.

    """
    a = - p * np.log2(p) if p > 0 else 0
    b = - (1 - p) * np.log2(1 - p) if p < 1 else 0
    return a + b


def _derivative_binary_entropy(x):
    """Derivative of binary-entropy function.

    Parameters
    ----------
    x : float in the open interval (0, 1)
        Value at which to evaluate the function.

    Return
    ------
    float

    """
    return - np.log2(x / (1 - x))


def _error_binary_entropy(x, x_error):
    """Standard error in the binary-entropy function.

    Parameters
    ----------
    x : float in the closed interval [0, 1]
        Estimated value of the argument.
    x_error : float
        Standard error of the argument.

    Return
    ------
    float

    Note
    ----
    Only accurate when x_error << 1.

    """
    if np.isclose(x, 0) or np.isclose(x, 1) or x_error == 0:
        return 0
    return _derivative_binary_entropy(x) * x_error


def _error_secret_key_rate(qber_x, qber_x_error, qber_z, qber_z_error,
                           attempts_per_success, attempts_per_success_error):
    """Calculating the standard error in the secret-key rate.

    Parameters
    ----------
    qber_x : float (between 0 and 1)
        Quantum Bit Error Rate in X basis (both Alice and Bob measure in X)
    qber_x_error : float
        Standard deviation of the Quantum Bit Error Rate in X basis
    qber_z : float (between 0 and 1)
        Quantum Bit Error Rate in Z basis (both Alice and Bob measure in Z)
    qber_z_error : float
        Standard deviation of the Quantum Bit Error Rate in Z basis
    attempts_per_success: float
        Average number of attempts required per successfully-distributed raw key bit.
    attempts_per_success_error: float
        Standard deviation of the number of attempts required per successfully-distributed raw key bit.

    Returns
    -------
    float

    """
    secret_key_rate = 1 - _binary_entropy(qber_x) - _binary_entropy(qber_z)
    if secret_key_rate <= 0:
        return 0
    return (np.sqrt(
        np.power(_error_binary_entropy(qber_x, qber_x_error), 2) +
        np.power(_error_binary_entropy(qber_z, qber_z_error), 2) +
        np.power(secret_key_rate * attempts_per_success_error, 2)
    ) / attempts_per_success)


def _secret_key_rate(qber_x, qber_x_error, qber_z, qber_z_error,
                     attempts_per_success, attempts_per_success_error):
    """ Function that compute the secret key rate and its standard deviation from the simulation data in the input
    Dataframe.

    This is done by calculating the Quantum Bit Error Rate (QBER) in X and Z basis anf then taking:
    max(0., 1 - H(qber_x) - H(qber_z))

    where H(p) is the binary entropy function:
    H(p) = -p log(p) - (1-p) log(1-p)

    Parameters
    ----------
    qber_x : float (between 0 and 1)
        Quantum Bit Error Rate in X basis (both Alice and Bob measure in X)
    qber_x_error : float
        Standard deviation of the Quantum Bit Error Rate in X basis
    qber_z : float (between 0 and 1)
        Quantum Bit Error Rate in Z basis (both Alice and Bob measure in Z)
    qber_z_error : float
        Standard deviation of the Quantum Bit Error Rate in Z basis
    attempts_per_success: float
        Average number of attempts required per successfully-distributed raw key bit.
    attempts_per_success_error: float
        Standard deviation of the number of attempts required per successfully-distributed raw key bit.


    Return
    ------
    secret_key_rate : float
        Secret Key Rate (unit-less) as max(0., 1 - H(qber_x) - H(qber_z)).
    skr_min : float
        Minimal value of Secret Key Rate within the interval given by the Stdev of the Qber.
    skr_max : float
        Maximal value of Secret Key Rate within the interval given by the Stdev of the Qber.
    skr_error : float
        Standard error in the secret-key rate (calculated using standard formula for propagation of uncertainty).

    """
    # calculate secret key rate using binary entropy function
    secret_key_rate = max(0., 1 - _binary_entropy(qber_x) - _binary_entropy(qber_z))

    if (qber_x, qber_z) <= (0, 1):
        print("Beware: one of the QBER's is 0 or 1. "
              "This may indicate not enough statistics were obtained to estimate the error.")

    # pick min / max in interval [0,1]
    skr_min = min(1 - _binary_entropy(qber_x + qber_x_error) - _binary_entropy(qber_z + qber_z_error),
                  1 - _binary_entropy(qber_x + qber_x_error) - _binary_entropy(qber_z - qber_z_error),
                  1 - _binary_entropy(qber_x - qber_x_error) - _binary_entropy(qber_z + qber_z_error),
                  1 - _binary_entropy(qber_x - qber_x_error) - _binary_entropy(qber_z - qber_z_error),
                  1 - _binary_entropy(qber_x) - _binary_entropy(qber_z),
                  1.)
    skr_min = max(0., skr_min)
    skr_max = max(1 - _binary_entropy(qber_x + qber_x_error) - _binary_entropy(qber_z + qber_z_error),
                  1 - _binary_entropy(qber_x + qber_x_error) - _binary_entropy(qber_z - qber_z_error),
                  1 - _binary_entropy(qber_x - qber_x_error) - _binary_entropy(qber_z + qber_z_error),
                  1 - _binary_entropy(qber_x - qber_x_error) - _binary_entropy(qber_z - qber_z_error),
                  1 - _binary_entropy(qber_x) - _binary_entropy(qber_z),
                  0.)
    skr_max = min(1., skr_max)

    skr_error = _error_secret_key_rate(qber_x, qber_x_error, qber_z, qber_z_error,
                                       attempts_per_success, attempts_per_success_error)

    return secret_key_rate, skr_min, skr_max, skr_error


def _do_we_expect_correlation(row):
    """Function that computes the expected correlation of measurement results between Alice and Bob for a row of the
    input DataFrame.

    This is done by calculating the expected target state and then looking up its expected correlation for the basis
    choice specified in this row of the DataFrame.

    Parameters
    ----------
    row : pandas.Series
        Row of the DataFrame containing simulation data


    Return
    ------
    bool
        Boolean, whether we expect measurement results between Alice and Bob to be correlated.

    Note: For indexing of Bell stated see `_expected_target_state()`

    """
    basis_a = row["basis_A"]
    basis_b = row["basis_B"]
    if basis_a != basis_b:
        # TODO: maybe this should not throw an error but just ignore the line?
        raise ValueError("Cannot get correlation for Bell states if A and B measured in different basis.")

    # when using atomic ensembles with presence-absence encoding, two chains have to be used. The expected correlation
    # depends on the target state of both chains
    if 'chain_2_midpoint_0' in row.keys():
        if basis_a == "X":
            expected_index_chain_1, expected_index_chain_2 = _expected_target_state(row)
            if expected_index_chain_1 == expected_index_chain_2:
                return True
            else:
                return False
        elif basis_a == "Z":
            return False
        elif basis_a == "Y":
            raise NotImplementedError("Y-basis correlations not implemented for two chain AE setup yet.")

    expected_target_index = _expected_target_state(row)
    basis_to_is_correlated = {"X": {BellIndex.PHI_PLUS: True,
                                    BellIndex.PSI_PLUS: True,
                                    BellIndex.PSI_MINUS: False,
                                    BellIndex.PHI_MINUS: False},
                              "Y": {BellIndex.PHI_PLUS: False,
                                    BellIndex.PSI_PLUS: True,
                                    BellIndex.PSI_MINUS: False,
                                    BellIndex.PHI_MINUS: True},
                              "Z": {BellIndex.PHI_PLUS: True,
                                    BellIndex.PSI_PLUS: False,
                                    BellIndex.PSI_MINUS: False,
                                    BellIndex.PHI_MINUS: True},
                              }
    return basis_to_is_correlated[basis_a][expected_target_index]


def _expected_target_state(row):
    """ Function that computes the expected target state shared between Alice and Bob for a single row of the input
    DataFrame.

    This is done by looking at all swap and midpoint outcomes and applying the corresponding Pauli corrections to the
    |Phi_+> state.

    Note:
    As all currently all sources create the same Bell states and there is always an even number of sources in a
    repeater chain (2 per elementary link), this holds for any Bell state the sources create..

    Parameters
    ----------
    row : pandas.Series
        Row of the DataFrame containing simulation data.

    Return
    ------
    bell_index : :class:`netsquid.qubits.ketstates.BellIndex`
        Bell index of the expected target state that Alice and Bob share.

    """
    # extract all midpoint and swap outcomes and combine them in a list
    all_outcomes = []

    for column_name, value in row.iteritems():
        split_column_name = column_name.split("_")
        if len(split_column_name) > 1:
            if column_name.split("_")[1] == "outcome" and column_name.split("_")[0] in ["midpoint", "swap"]:
                all_outcomes.append(value)

    # count number of Pauli corrections and using: sigma_i . sigma_i = identity
    x = all_outcomes.count(BellIndex.PSI_PLUS) % 2
    y = all_outcomes.count(BellIndex.PSI_MINUS) % 2
    z = all_outcomes.count(BellIndex.PHI_MINUS) % 2

    # create 3d array that maps x,y,z to bell index
    # x | y | z | effect
    # ------------------
    # 0   0   0 |  0 > 0
    # 1   0   0 |  0 > 1
    # 0   1   0 |  0 > 2
    # 1   1   0 |  0 > 3
    # 0   0   1 |  0 > 3
    # 1   0   1 |  0 > 2
    # 0   1   1 |  0 > 1
    # 1   1   1 |  0 > 0

    bell_index = [[[BellIndex.PHI_PLUS, BellIndex.PHI_MINUS], [BellIndex.PSI_MINUS, BellIndex.PSI_PLUS]],
                  [[BellIndex.PSI_PLUS, BellIndex.PSI_MINUS], [BellIndex.PHI_MINUS, BellIndex.PHI_PLUS]]]

    # when using atomic ensembles with presence-absence encoding, two chains have to be used. The expected correlation
    # depends on the target state of both chains
    if 'chain_2_midpoint_0' in row.keys():
        all_outcomes_chain_2 = []
        for column_name, value in row.iteritems():
            split_column_name = column_name.split("_")
            if len(split_column_name) > 1:
                if column_name.split("_")[0] == "chain" and column_name.split("_")[1] == "2" and \
                   column_name.split("_")[2] in ["midpoint", "swap"]:
                    all_outcomes_chain_2.append(value)
        # check of both chains had the same length
        if len(all_outcomes_chain_2) != len(all_outcomes):
            raise ValueError(f"Chain 1 had {len(all_outcomes)} while chain 2 had {len(all_outcomes_chain_2)}")
        x_2 = all_outcomes_chain_2.count(BellIndex.PSI_PLUS) % 2
        y_2 = all_outcomes_chain_2.count(BellIndex.PSI_MINUS) % 2
        z_2 = all_outcomes_chain_2.count(BellIndex.PHI_MINUS) % 2
        return bell_index[x][y][z], bell_index[x_2][y_2][z_2]

    return bell_index[x][y][z]
